#Ejecutar el siguiente update para poder asignar nueva primary key a tabla equipo
#1062 - Entrada duplicada '1659-2021-01-12 09:49:25' para la clave 'PRIMARY'
update equipo set fhasta='2021-01-12 09:49:28' where idEquipo=1659 and fdesde='2021-01-12 09:40:46';
ALTER TABLE equipo DROP PRIMARY KEY, ADD PRIMARY KEY (idEquipo, fhasta) USING BTREE;

ALTER TABLE glpi DROP PRIMARY KEY, ADD PRIMARY KEY (id, fhasta) USING BTREE;
ALTER TABLE materiales DROP PRIMARY KEY, ADD PRIMARY KEY (id, fhasta) USING BTREE;
ALTER TABLE oficina DROP PRIMARY KEY, ADD PRIMARY KEY (id, fhasta) USING BTREE;
ALTER TABLE software DROP PRIMARY KEY, ADD PRIMARY KEY (id, fhasta) USING BTREE;
ALTER TABLE tdepartamentos DROP PRIMARY KEY, ADD PRIMARY KEY (id, fhasta) USING BTREE;
ALTER TABLE tusuarios_lab DROP PRIMARY KEY, ADD PRIMARY KEY (usuario, fhasta) USING BTREE;
ALTER TABLE tusuarios_pc DROP PRIMARY KEY, ADD PRIMARY KEY (id, fhasta) USING BTREE;
ALTER TABLE tzonas DROP PRIMARY KEY, ADD PRIMARY KEY (id, fhasta) USING BTREE;

#Aumenta tamano columna contrasena
ALTER TABLE `tusuarios_lab` CHANGE `contraseña` `contraseña` VARCHAR(60) CHARACTER 
SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL;
UPDATE `tusuarios_lab` SET `contraseña` = '$2a$10$1h6doQ3leV8dQPxVIZUe7.JHWV4ViPuMVcDzCQvl2QAEM0R8fpyfG' WHERE `tusuarios_lab`.`usuario` = 'admin' AND `tusuarios_lab`.`fhasta` = '2999-12-31 00:00:00';


ALTER TABLE equipo
  ADD oficina_id int(11) after ubicacion;
ALTER TABLE equipo
  ADD CONSTRAINT `FK_EQ_OFICINA` FOREIGN KEY (oficina_id) REFERENCES oficina (id) ON UPDATE CASCADE;
update equipo eq set oficina_id=(select toe.idOficina from toficina_equipos toe where fhasta='2999-12-31 00:00:00' and 
toe.idEquipo=eq.idEquipo) where eq.fhasta='2999-12-31';

ALTER TABLE equipo
  ADD departamento_id int(11) after oficina_id;
ALTER TABLE equipo
  ADD CONSTRAINT `FK_EQ_DEPARTAMENTO` FOREIGN KEY (departamento_id) REFERENCES tdepartamentos (id) ON UPDATE CASCADE;
update equipo eq set departamento_id=(select td.id from (select nombre,id from tdepartamentos group by nombre, id ) td, toficina_equipos toe
where toe.fhasta='2999-12-31' and td.nombre=toe.departamento
and toe.idEquipo=eq.idEquipo) where eq.fhasta='2999-12-31';


--Se modifica nombres de columnas y tablas
RENAME TABLE equipo TO tequipo;
ALTER TABLE tequipo CHANGE COLUMN idEquipo  cequipo  INT( 11 ) NOT NULL AUTO_INCREMENT;
ALTER TABLE tequipo CHANGE COLUMN oficina_id  coficina INT( 11 );
ALTER TABLE tequipo CHANGE COLUMN departamento_id  cdepartamento INT( 11 );
ALTER TABLE tequipo CHANGE COLUMN codActivo  cactivo varchar( 22 );

RENAME TABLE oficina TO toficina;
ALTER TABLE toficina CHANGE COLUMN id  coficina  INT( 11 ) NOT NULL AUTO_INCREMENT;

RENAME TABLE tdepartamentos TO tdepartamento;
ALTER TABLE tdepartamento CHANGE COLUMN id  cdepartamento  INT( 11 ) NOT NULL AUTO_INCREMENT;

RENAME TABLE glpi TO tglpi;
ALTER TABLE tglpi CHANGE COLUMN id  cglpi  INT( 11 ) NOT NULL AUTO_INCREMENT;
ALTER TABLE tglpi CHANGE COLUMN idEquipo  cequipo  INT( 11 );
ALTER TABLE tglpi CHANGE COLUMN num_glpi  numero  varchar( 10 );

RENAME TABLE materiales TO tmaterial;
ALTER TABLE tmaterial CHANGE COLUMN id  cmaterial  INT( 11 ) NOT NULL AUTO_INCREMENT;
ALTER TABLE tmaterial CHANGE COLUMN idZona  czona  INT( 11 );

RENAME TABLE software TO tsoftware;
ALTER TABLE tsoftware CHANGE COLUMN id  csoftware  INT( 11 ) NOT NULL AUTO_INCREMENT;

RENAME TABLE tusuarios_lab TO tusuarios;
ALTER TABLE tusuarios CHANGE COLUMN cusuario  cusuario_modificacion  varchar( 30 )  ;
ALTER TABLE tusuarios CHANGE COLUMN usuario  cusuario  varchar( 30 ) NOT NULL ;
ALTER TABLE tusuarios CHANGE COLUMN idZona  czona  int( 11 )  ;
ALTER TABLE tusuarios CHANGE COLUMN id_tipo  ctipousuario  int( 11 )  ;
ALTER TABLE tusuarios CHANGE COLUMN contraseña  contraseña  varchar( 60 ) NOT NULL  ;

ALTER TABLE tmovimientos CHANGE COLUMN id  cmovimiento  INT( 11 ) NOT NULL AUTO_INCREMENT ;
ALTER TABLE tmovimientos CHANGE COLUMN idMaterial  cmaterial  INT( 11 ) NOT NULL ;
ALTER TABLE tmovimientos CHANGE COLUMN fecha  fmovimiento date NOT NULL DEFAULT current_timestamp() ;
ALTER TABLE tmovimientos DROP PRIMARY KEY, ADD PRIMARY KEY (cmovimiento, fhasta) USING BTREE;

RENAME TABLE ttipo_usuario TO ttipousuario;
ALTER TABLE ttipousuario CHANGE COLUMN id  ctipousuario  INT( 11 ) NOT NULL AUTO_INCREMENT;
ALTER TABLE ttipousuario CHANGE COLUMN nombre  descripcion  varchar( 255 ) NOT NULL;

RENAME TABLE tusuarios_pc TO tusuariospc;
ALTER TABLE tusuariospc CHANGE COLUMN id  cusuariopc  INT( 11 ) NOT NULL AUTO_INCREMENT;

ALTER TABLE tequipos_software CHANGE COLUMN idEquipo  cequipo  INT( 11 );
ALTER TABLE tequipos_software CHANGE COLUMN idSoftware  csoftware  INT( 11 );
ALTER TABLE tequipos_software CHANGE COLUMN usuarioPC  cusuariopc  varchar( 30 );
ALTER TABLE tequipos_software drop CONSTRAINT `FK_USUARIOPC_SOFTWARE`;
DROP INDEX FK_USUARIOPC_SOFTWARE ON tequipos_software;

ALTER TABLE tzonas CHANGE COLUMN id  czona  INT( 11 ) NOT NULL AUTO_INCREMENT;

ALTER TABLE tusuarios drop CONSTRAINT `FK_TIPO`;
DROP INDEX FK_TIPO ON tusuarios;
ALTER TABLE tusuarios CHANGE COLUMN ctipousuario  ctipousuario  varchar( 3 ) ;
ALTER TABLE ttipousuario CHANGE COLUMN ctipousuario  ctipousuario  varchar( 3 ) not null;
update ttipousuario set ctipousuario='ADM' where ctipousuario='1';
update ttipousuario set ctipousuario='USR' where ctipousuario='2';
update tusuarios set ctipousuario='ADM' where ctipousuario='1';
update tusuarios set ctipousuario = 'USR' where ctipousuario='2';
ALTER TABLE tusuarios
  ADD CONSTRAINT `FK_TIPO` FOREIGN KEY (ctipousuario) REFERENCES ttipousuario (ctipousuario) ON UPDATE CASCADE;

CREATE TABLE `tusuariocontrasena` (
  `cusuario` varchar(30) NOT NULL,  
  `fhasta` datetime NOT NULL DEFAULT '2999-12-31 00:00:00',
  `fdesde` datetime NOT NULL DEFAULT current_timestamp(),
  `contrasena` varchar(60) NOT NULL,
  `fcaducidad` datetime ,
  `temporal` varchar(1) DEFAULT '0',
  PRIMARY KEY (cusuario, fhasta), 
  CONSTRAINT FK_CONTRASENA_USUARIO FOREIGN KEY (cusuario) REFERENCES `tusuarios` (`cusuario`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `tusuariosesiones` (
  `cusuario` varchar(30) NOT NULL,  
  `fhasta` datetime NOT NULL DEFAULT '2999-12-31 00:00:00',
  `fdesde` datetime NOT NULL DEFAULT current_timestamp(),
  sesion varchar(60) NOT NULL,
  ipadress varchar(15) NOT NULL,
  PRIMARY KEY (cusuario, fhasta, sesion), 
  CONSTRAINT FK_SESION_USUARIO FOREIGN KEY (cusuario) REFERENCES `tusuarios` (`cusuario`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

insert into tusuariocontrasena 
select cusuario, fhasta, fdesde, contraseña, null,'0' from tusuarios;

insert into tusuariosesiones 
select cusuario, fhasta, fdesde, ultima_sesion, null from tusuarios;

alter table tusuarios drop column contraseña, drop column ultima_sesion;

alter table tusuariocontrasena modify temporal tinyint(1) default 0;