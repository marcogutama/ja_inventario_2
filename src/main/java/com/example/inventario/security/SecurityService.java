package com.example.inventario.security;

import com.example.inventario.backend.ApplicationDates;
import com.example.inventario.backend.entity.Tusuariosesiones;
import com.example.inventario.backend.entity.TusuariosesionesPK;
import com.example.inventario.backend.repository.TusuariosesionesRepository;
import com.vaadin.flow.spring.security.AuthenticationContext;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class SecurityService {
    private final AuthenticationContext authenticationContext;
    @Autowired
    private TusuariosesionesRepository repository;
    @Autowired
    private HttpServletRequest request;

    public SecurityService(AuthenticationContext authenticationContext) {
        this.authenticationContext = authenticationContext;
    }

    public UserDetails getAuthenticatedUser() {
        return authenticationContext.getAuthenticatedUser(UserDetails.class).get();
    }

    public void logout() {
        UserDetails userDetails = getAuthenticatedUser();
        HttpSession session = request.getSession();
        if (session != null) {
            System.out.println("Sesion log out "+userDetails.getUsername()+": " +session);
            TusuariosesionesPK tusuariosesionesPK = new TusuariosesionesPK();
            tusuariosesionesPK.setCusuario(userDetails.getUsername());
            tusuariosesionesPK.setFhasta(ApplicationDates.DEFAULT_EXPIRY_TIMESTAMP);
            tusuariosesionesPK.setSesion(session.toString());
            Optional<Tusuariosesiones> tusuariosesiones = repository.findById(tusuariosesionesPK);
            tusuariosesiones.ifPresent(usersesion ->repository.expire(usersesion));
        }
        authenticationContext.logout();
    }
}
