package com.example.inventario.security;

import com.example.inventario.backend.ApplicationDates;
import com.example.inventario.backend.entity.Tusuariosesiones;
import com.example.inventario.backend.repository.TusuariosesionesRepository;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.event.AbstractAuthenticationFailureEvent;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Log4j2
@Component
public class AuthenticationEvents {
    @Autowired
    private TusuariosesionesRepository repository;
    @Autowired
    private HttpServletRequest request;
    @Autowired
    HttpServletResponse response;
    @EventListener
    public void onSuccess(AuthenticationSuccessEvent success) {
        System.out.println("Ingreso exitoso");
        UserDetails userDetails = (UserDetails) success.getAuthentication().getPrincipal();
        HttpSession session = request.getSession(false);
        if (session != null) {
            System.out.println("Sesion: " +session);
            Tusuariosesiones tusuariosesiones = new Tusuariosesiones();
            tusuariosesiones.setCusuario(userDetails.getUsername());
            tusuariosesiones.setSesion(session.toString());
            tusuariosesiones.setIpadress(request.getRemoteAddr());
            tusuariosesiones.setFhasta(ApplicationDates.DEFAULT_EXPIRY_TIMESTAMP);
            tusuariosesiones.setFdesde(ApplicationDates.CURRENT_EXPIRY_TIMESTAMP);
            repository.save(tusuariosesiones);
        }
    }

    @EventListener
    public void onFailure(AbstractAuthenticationFailureEvent failures) throws IOException {
        HttpSession session = request.getSession(true);
        if(session != null && failures.getException() instanceof CredentialsExpiredException) {
            String userPass = failures.getAuthentication().getPrincipal().toString();
            log.error("Contraseña caducada, usuario : " + userPass);
            request.getSession().setAttribute("USER_EXPIRED_CREDENTIALS",userPass);
            response.sendRedirect("/change_password" );
        }
    }

}
