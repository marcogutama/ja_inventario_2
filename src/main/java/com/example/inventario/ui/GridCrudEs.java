package com.example.inventario.ui;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.grid.HeaderRow;
import com.vaadin.flow.component.textfield.TextField;
import org.vaadin.crudui.crud.CrudListener;
import org.vaadin.crudui.crud.CrudOperation;
import org.vaadin.crudui.crud.impl.GridCrud;
import org.vaadin.crudui.layout.impl.WindowBasedCrudLayout;

import java.util.HashMap;

/**
 * Clase que traduce los botones a español y adiciona
 * metodos para agregrar filtros de busqueda en el grid
 * @param <T>
 * @author Marco Gutama
 */
public class GridCrudEs<T> extends GridCrud<T> {

    /**
     * Header que contiene los filtros de las columnas
     */
    private HeaderRow headerRowSearch;

    /**
     * Traduce gridcrud a español
     * @param domainType
     * @param crudListener
     */
    public GridCrudEs(Class<T> domainType, CrudListener<T> crudListener) {
        super(domainType, crudListener);
        CustomCrudFormFactory formFactory = new CustomCrudFormFactory(domainType);
        formFactory.setButtonCaption(CrudOperation.ADD, "Agregar");
        formFactory.setButtonCaption(CrudOperation.UPDATE, "Actualizar");
        formFactory.setButtonCaption(CrudOperation.DELETE, "Caducar");
        formFactory.setCancelButtonCaption("Cancelar");
        WindowBasedCrudLayout a = (WindowBasedCrudLayout) this.getCrudLayout();
        a.setWindowCaption(CrudOperation.ADD, "Agregar");
        a.setWindowCaption(CrudOperation.UPDATE, "Actualizar");
        a.setWindowCaption(CrudOperation.DELETE, "¿Esta seguro de caducar este registro?");

        this.crudLayout = a;
        this.setCrudFormFactory(formFactory);
        this.getFindAllButton().getElement().setAttribute("title", "Buscar/Refrescar lista");
        this.getAddButton().getElement().setAttribute("title", "Agregar");
        this.getUpdateButton().getElement().setAttribute("title", "Actualizar");
        this.getDeleteButton().getElement().setAttribute("title", "Caducar");
        this.getGrid().addThemeVariants(GridVariant.LUMO_COMPACT);
        this.getGrid().addThemeVariants(GridVariant.LUMO_ROW_STRIPES);
    }

    /**
     * Agrega filtro en en header del grid
     * @param columnKey
     */
    public void agregarFiltro(String columnKey){
        if(this.getGrid().getHeaderRows().size()==1){
            headerRowSearch = this.getGrid().appendHeaderRow();
        }else{
            headerRowSearch =(HeaderRow) this.getGrid().getHeaderRows().get(1);
        }
        headerRowSearch.getCell(this.getGrid().getColumnByKey(columnKey)).setComponent(setupFiltro(new TextField()));
        headerRowSearch.getCell(this.getGrid().getColumnByKey(columnKey)).getComponent().toString();
    }

    /**
     * Da formato a los filtros
     * @param searchField
     * @return
     */
    public Component setupFiltro(TextField searchField){
        searchField.setPlaceholder("Filtro");
        searchField.setWidthFull();
        searchField.getStyle().set("max-width", "100%");
        searchField.setClearButtonVisible(true);
        return searchField;
    }

    /**
     * Obtnener los filtros de busqueda
     * @return parametrosBusqueda
     */
    public HashMap<String, Object> getFiltros(){
        HashMap<String, Object> parametrosBusqueda = new HashMap<String, Object>();
        if (headerRowSearch != null) {
            this.getGrid().getColumns().forEach(c -> {
                String columnKey = c.getKey();
                Component component = headerRowSearch.getCell(this.getGrid().getColumnByKey(columnKey)).getComponent();
                if (component instanceof TextField txtFiltro && !txtFiltro.isEmpty()) {
                    parametrosBusqueda.put(columnKey, txtFiltro.getValue());
                }
            });
        }
        return parametrosBusqueda;
    }
}
