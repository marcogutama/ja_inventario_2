package com.example.inventario.ui;

import com.example.inventario.backend.entity.Tusuariocontrasena;
import com.example.inventario.backend.service.UsuariocontrasenaService;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.*;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinSession;
import com.vaadin.flow.server.auth.AnonymousAllowed;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

@Log4j2
@Route("change_password")
@PageTitle("Cambiar contraseña")
@AnonymousAllowed
public class ChangePasswordView extends VerticalLayout {
    @Autowired
    private PasswordEncoder passwordEncoder;
    PasswordField current_pass = new PasswordField("Contraseña actual");
    PasswordField new_pass = new PasswordField("Contraseña nueva");
    PasswordField confirmation_pass = new PasswordField("Confirmar contraseña");
    Tusuariocontrasena userPass;

    public ChangePasswordView(UsuariocontrasenaService contrasenaService) {
        if (VaadinSession.getCurrent().getSession().getAttribute("USER_EXPIRED_CREDENTIALS") != null) {
        String cusuario = VaadinSession.getCurrent().getSession().getAttribute("USER_EXPIRED_CREDENTIALS").toString();
        log.info("Usuario a cambiar contraseña: " + cusuario);

            userPass = contrasenaService.findByCusuario(cusuario);
            configurePasswordFields();
            setSizeFull();
            setJustifyContentMode(JustifyContentMode.CENTER);
            setAlignItems(Alignment.CENTER);
            H4 h4User = new H4(userPass.getCusuario());
            this.setMargin(false);
            this.setPadding(false);

            add(
                    new H2("Actualizar contraseña"),
                    //new H2(userPass.getCusuario()),
                    current_pass,
                    new_pass,
                    confirmation_pass,
                    createButtonLayout(contrasenaService)
            );
        }
    }

    private void configurePasswordFields() {
        new_pass.setRequiredIndicatorVisible(true);
        new_pass.setPattern("^(|(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,})$");
        new_pass.setMinLength(6);
        new_pass.setMaxLength(12);
        new_pass.setHelperText("necesita 6 o más caracteres, mezclando dígitos, letras minúsculas y mayúsculas");
        //new_pass.setErrorMessage("necesita 6 o más caracteres, mezclando dígitos, letras minúsculas y mayúsculas");

        confirmation_pass.setRequiredIndicatorVisible(true);
        confirmation_pass.setRequired(true);

        current_pass.setRequiredIndicatorVisible(true);
        current_pass.setRequired(true);
    }

    private Component createButtonLayout(UsuariocontrasenaService contrasenaService) {
        Button btGuardar = new Button("Guardar", e -> {
            if (!current_pass.isEmpty() && !new_pass.isEmpty() && !confirmation_pass.isEmpty()) {
                if (passwordEncoder.matches(current_pass.getValue(), userPass.getContrasena())) {
                    if (confirmation_pass.getValue().equals(new_pass.getValue())) {
                        userPass.setContrasena(passwordEncoder.encode(new_pass.getValue()));
                        userPass.setTemporal((byte) 0);
                        contrasenaService.update(userPass);
                        Notification.show("Se actualizo correctamente la contraseña");
                        log.info("Se actualizo correctamente la contraseña, usuario " + userPass.getCusuario());
                        VaadinSession.getCurrent().getSession().setAttribute("USER_EXPIRED_CREDENTIALS", null);
                        getUI().ifPresent(ui ->ui.navigate(LoginView.class));

                    } else {
                        Notification.show("No coinciden las contraseñas");
                    }
                } else {
                    Notification.show("Contraseña actual incorrecta");
                }
            } else {
                Notification.show("Debe llenar todos los campos");
            }
        });
        Button btCancelar = new Button("Cancelar");

        btGuardar.addThemeVariants(ButtonVariant.LUMO_CONTRAST);
        btCancelar.addThemeVariants(ButtonVariant.LUMO_TERTIARY);

        btGuardar.addClickShortcut(Key.ENTER);
        btCancelar.addClickShortcut(Key.ESCAPE);
        return new HorizontalLayout(btGuardar, btCancelar);
    }
}
