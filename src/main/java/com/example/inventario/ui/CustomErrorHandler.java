package com.example.inventario.ui;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.server.ErrorEvent;
import com.vaadin.flow.server.ErrorHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CustomErrorHandler implements ErrorHandler {

    private static final Logger logger = LoggerFactory.getLogger(CustomErrorHandler.class);

    @Override
    public void error(ErrorEvent errorEvent) {
        logger.error("Something wrong happened", errorEvent.getThrowable());
        if(UI.getCurrent() != null) {
            UI.getCurrent().access(() -> {
                String trace = "";
                for (StackTraceElement s : errorEvent.getThrowable()
                        .getStackTrace()) {
                    trace = trace + "  at " + s
                            + "\n";
                }
                //errorEvent.getThrowable().printStackTrace();
                Dialog dialog = new Dialog();

                dialog.setHeaderTitle("Error");
                dialog.add("Internal Error: " + errorEvent.getThrowable().getMessage());
                String finalTrace = trace;
                Button deleteButton = new Button("Detalle>>", (e) -> {
                    dialog.setWidth("600px");
                    dialog.setHeight("500px");
                    dialog.add(finalTrace);
                });
                deleteButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY,
                        ButtonVariant.LUMO_ERROR);
                deleteButton.getStyle().set("margin-right", "auto");

                Button cancelButton = new Button("OK", (e) -> dialog.close());
                cancelButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);

                dialog.getFooter().add(deleteButton,cancelButton);
                dialog.open();
            });
        }
    }
}
