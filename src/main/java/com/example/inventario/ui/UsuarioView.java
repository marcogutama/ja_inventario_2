package com.example.inventario.ui;

import com.example.inventario.backend.ApplicationDates;
import com.example.inventario.backend.entity.Tusuariocontrasena;
import com.example.inventario.backend.entity.Tusuarios;
import com.example.inventario.backend.service.TipousuarioService;
import com.example.inventario.backend.service.UsuarioService;
import com.example.inventario.backend.service.UsuariocontrasenaService;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.open.App;
import jakarta.annotation.security.RolesAllowed;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.vaadin.crudui.form.impl.field.provider.ComboBoxProvider;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

@Route(value = "usuarios", layout = MainView.class)
@RolesAllowed("ADM")
@PageTitle("Usuarios")
//@JsModule("./copytoclipboard.js")
public class UsuarioView extends VerticalLayout {
    GridCrudEs<Tusuarios> crud;

    public UsuarioView(UsuarioService service, UsuariocontrasenaService contrasenaService, TipousuarioService tipousuarioService) {
        crud = new GridCrudEs<>(Tusuarios.class, service);
        crud.getGrid().setColumns("cusuario", "nombre", "czona", "ctipousuario");

        //Button for reset password
        Button resetButton = new Button("Asignar/Resetear contraseña");
        resetButton.setEnabled(false);
        crud.getGrid().addSelectionListener(e->resetButton.setEnabled(!crud.getGrid().asSingleSelect().isEmpty()));

        resetButton.addClickListener(clickEvent -> {
            resetPassDialog((Tusuarios) crud.getGrid().asSingleSelect().getValue(), contrasenaService).open();
        });
        crud.getCrudLayout().addToolbarComponent(resetButton);

        crud.getCrudFormFactory().setVisibleProperties("cusuario", "nombre", "czona", "ctipousuario");
        crud.getCrudFormFactory().setFieldProvider("ctipousuario", new ComboBoxProvider<>(new ArrayList<String>(tipousuarioService.findAll().stream().map(tipousuario -> tipousuario.toString()).toList())));

        crud.agregarFiltro("cusuario");
        crud.getGrid().getColumns().forEach(col -> col.setAutoWidth(true));
        crud.getCrudFormFactory().setUseBeanValidation(true);

        add(crud);
        crud.setOperations(
                () -> service.findAll(crud.getFiltros()),
                tusuarios -> service.add((Tusuarios) tusuarios),
                tusuarios -> service.update((Tusuarios) tusuarios),
                tusuarios -> service.delete((Tusuarios) tusuarios)
        );
    }
    public Dialog resetPassDialog(Tusuarios usuario, UsuariocontrasenaService service){
        TextField usuarioTxt = new TextField("Usuario");
        usuarioTxt.setValue(usuario.getCusuario());
        TextField passTxt = new TextField("Contraseña temporal");
        passTxt.setValue(RandomStringUtils.random(8, 0, 122, true, true));
        Button copyBt = new Button("", VaadinIcon.COPY.create());
        copyBt.addClickListener(
                e -> UI.getCurrent().getPage().executeJs("window.copyToClipboard($0)", passTxt.getValue())
        );
        HorizontalLayout hLayout= new HorizontalLayout(passTxt, copyBt);
        hLayout.setAlignItems(Alignment.BASELINE);
        VerticalLayout dialogLayout = new VerticalLayout(usuarioTxt, hLayout);
        dialogLayout.setWidth("100%");
        dialogLayout.setMargin(false);
        dialogLayout.setPadding(false);

        Dialog dialog = new Dialog(new H3("Asignar/Resetear contraseña"), dialogLayout);
        Button resetSaveButton = createResetSaveButton(dialog, usuarioTxt.getValue(), passTxt.getValue(), service);
        Button cancelButton = new Button("Cancelar", e -> dialog.close());
        dialog.getFooter().add(cancelButton);
        dialog.getFooter().add(resetSaveButton);

        return dialog;
    }

    private static Button createResetSaveButton(Dialog dialog, String usuarioTxt, String password, UsuariocontrasenaService service) {
        Tusuariocontrasena usuario = new Tusuariocontrasena();
        Button saveButton = new Button("Resetear", e -> {
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            usuario.setCusuario(usuarioTxt);
            usuario.setFhasta(ApplicationDates.DEFAULT_EXPIRY_TIMESTAMP);
            usuario.setContrasena(passwordEncoder.encode(password));
            usuario.setTemporal((byte) 1);
            usuario.setFdesde(ApplicationDates.CURRENT_EXPIRY_TIMESTAMP);
            service.add(usuario);
            dialog.close();
        });
        saveButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        return saveButton;
    }
}
