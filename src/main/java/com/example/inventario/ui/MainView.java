package com.example.inventario.ui;

import java.util.Optional;

import com.example.inventario.security.SecurityService;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.component.tabs.TabsVariant;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouterLink;
import com.vaadin.flow.server.VaadinSession;
import com.vaadin.flow.theme.lumo.LumoUtility;
import jakarta.annotation.security.PermitAll;


/**
 * The main view is a top-level placeholder for other views.
 */
//
// @JsModule("./styles/shared-styles.js")
//@Theme(value = Lumo.class, variant = Lumo.LIGHT)
//@CssImport("./styles/views/main/main-view.css")
//@PWA(name = "Entorno", shortName = "Entorno")
@Route("")
@PermitAll
public class MainView extends AppLayout {
    private final SecurityService securityService;

    private final Tabs menu;

    public MainView(SecurityService securityService) {
        this.securityService = securityService;
        Image img = new Image("images/logo2.png", "Avatar");
        img.setHeight("44px");
        menu = createMenu();
        Button logout = new Button(new Icon(VaadinIcon.SIGN_OUT) , e -> securityService.logout());
        logout.setTooltipText("Salir");

        var header = new HorizontalLayout(img, menu, logout);
        header.setDefaultVerticalComponentAlignment(FlexComponent.Alignment.CENTER);
        header.expand(menu);
        header.setWidthFull();
        header.addClassNames(
                LumoUtility.Padding.Vertical.NONE,
                LumoUtility.Padding.Horizontal.MEDIUM);

        addToNavbar(header);
        VaadinSession.getCurrent().setErrorHandler(new CustomErrorHandler());
    }

    private Tabs createMenu() {
        final Tabs tabs = new Tabs();
        tabs.addThemeVariants(TabsVariant.LUMO_MINIMAL);
        tabs.setId("tabs");
        tabs.add(createMenuItems());
        tabs.setSelectedTab(null);
        return tabs;
    }

    private Component[] createMenuItems() {
        return new Tab[] {
                createTab("Oficinas", OfficeView.class),
                createTab("Equipos", HardwareView.class),
                createTab("Usuarios", UsuarioView.class),
                createTab("About", OfficeView.class)
        };
    }

    private static Tab createTab(String text, Class<? extends Component> navigationTarget) {
        final Tab tab = new Tab();
        tab.add(new RouterLink(text, navigationTarget));
        ComponentUtil.setData(tab, Class.class, navigationTarget);
        return tab;
    }

    @Override
    protected void afterNavigation() {
        super.afterNavigation();
        getTabForComponent(getContent()).ifPresent(menu::setSelectedTab);
    }

    private Optional<Tab> getTabForComponent(Component component) {
        return menu.getChildren()
                .filter(tab -> ComponentUtil.getData(tab, Class.class)
                        .equals(component.getClass()))
                .findFirst().map(Tab.class::cast);
    }
}
