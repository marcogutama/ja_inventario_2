package com.example.inventario.ui;

import com.example.inventario.backend.entity.Tusuarios;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;


public class ResetPassView extends FormLayout {
    Button btGuardar = new Button("Guardar");
    Button btCancelar = new Button("Cancelar");
    TextField usuario = new TextField("Usuario");
    PasswordField pass = new PasswordField("Contraseña nueva");
    PasswordField pass_confirmacion = new PasswordField("Confirmar contraseña");
    public ResetPassView(Tusuarios tusuarios) {
        if (tusuarios!=null) {
            usuario.setValue(tusuarios.getCusuario());
            add(
                    usuario,
                    pass,
                    pass_confirmacion,
                    createButtonLayout()
            );
        }
    }
    private Component createButtonLayout() {
        btGuardar.addThemeVariants(ButtonVariant.LUMO_CONTRAST);
        btCancelar.addThemeVariants(ButtonVariant.LUMO_TERTIARY);

        btGuardar.addClickShortcut(Key.ENTER);
        btCancelar.addClickShortcut(Key.ESCAPE);
        return new HorizontalLayout(btGuardar, btCancelar);
    }
}
