package com.example.inventario.ui;

import com.example.inventario.backend.entity.Toficina;
import com.example.inventario.backend.service.OficinaService;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import jakarta.annotation.security.RolesAllowed;
import lombok.extern.log4j.Log4j2;

import java.util.HashMap;

@Route(value = "oficinas", layout = MainView.class)
@RolesAllowed("ADM")
@PageTitle("Oficinas")
@Log4j2
public class OfficeView extends VerticalLayout {
    GridCrudEs<Toficina> crud;

    public OfficeView(OficinaService service) {

        crud= new GridCrudEs<>(Toficina.class, service);
        crud.getGrid().setColumns("nombre", "abreviatura", "zona");
        crud.getCrudFormFactory().setUseBeanValidation(true);
        crud.getCrudFormFactory().setVisibleProperties("nombre", "abreviatura", "zona");
        crud.agregarFiltro("nombre");
        crud.agregarFiltro("zona");
        crud.getGrid().getColumns().forEach(col -> col.setAutoWidth(true));
        crud.getCrudFormFactory().setUseBeanValidation(true);

        add( crud);
        crud.setOperations(
                () -> {
                    log.error("Mensaje prueba error");
                    log.info("Info mensaje log");
                    return service.findAll(crud.getFiltros());
                },
                oficina -> service.add((Toficina) oficina),
                oficina -> service.update((Toficina) oficina),
                oficina -> service.delete((Toficina) oficina)
        );
    }

}
