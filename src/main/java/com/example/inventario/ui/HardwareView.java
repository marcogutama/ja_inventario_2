package com.example.inventario.ui;

import com.example.inventario.backend.entity.Tequipo;
import com.example.inventario.backend.entity.Toficina;
import com.example.inventario.backend.entity.Tdepartamento;
import com.example.inventario.backend.service.DepartamentoService;
import com.example.inventario.backend.service.EquipoService;
import com.example.inventario.backend.service.OficinaService;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.renderer.TextRenderer;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import jakarta.annotation.security.PermitAll;
import org.vaadin.crudui.form.impl.field.provider.ComboBoxProvider;

import java.util.ArrayList;
import java.util.Arrays;

@Route(value = "equipos", layout = MainView.class)
@PermitAll
@PageTitle("Equipos")
public class HardwareView extends VerticalLayout {
    GridCrudEs<Tequipo> crud;
    public HardwareView(EquipoService service, OficinaService oficinaService, DepartamentoService departamentoService) {
        crud = new GridCrudEs<>(Tequipo.class, service);
        crud.getGrid().setColumns("cactivo", "tipo", "marca", "modelo", "serial", "procesador", "ram",
                "discoduro", "nombre", "mac", "ip", "estado", "observacion", "ubicacion", "oficina", "departamento");
        crud.getCrudFormFactory().setVisibleProperties("cactivo", "tipo", "marca", "modelo", "serial", "procesador", "ram", "discoduro",
                "nombre", "mac", "ip", "estado", "observacion", "ubicacion", "oficina", "departamento");
        crud.getCrudFormFactory().setFieldProvider("oficina",
                new ComboBoxProvider<>("Oficina", oficinaService.findAll(), new TextRenderer<>(Toficina::getNombre), Toficina::getNombre));
        crud.getCrudFormFactory().setFieldProvider("departamento",
                new ComboBoxProvider<>("Departamento", departamentoService.findAll(), new TextRenderer<>(Tdepartamento::getNombre), Tdepartamento::getNombre));
        crud.getCrudFormFactory().setFieldProvider("estado", new ComboBoxProvider<>(new ArrayList<String>(Arrays.asList("A", "B", "D", "R"))));

        crud.agregarFiltro("cactivo");
        crud.getGrid().getColumns().forEach(col -> col.setAutoWidth(true));
        crud.getCrudFormFactory().setUseBeanValidation(true);

        add(crud);
        crud.setOperations(
                () -> service.findAll(crud.getFiltros()),
                equipo -> service.add((Tequipo) equipo),
                equipo -> service.update((Tequipo) equipo),
                equipo -> service.delete((Tequipo) equipo)
        );

    }
}
