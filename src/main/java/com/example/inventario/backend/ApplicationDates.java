package com.example.inventario.backend;

import java.sql.Timestamp;
import java.util.Date;

public final class ApplicationDates {
    public static final Timestamp DEFAULT_EXPIRY_TIMESTAMP = Timestamp.valueOf("2999-12-31 00:00:00");
    public static final Timestamp CURRENT_EXPIRY_TIMESTAMP = new Timestamp((new Date()).getTime());

    public ApplicationDates() {
    }

}
