package com.example.inventario.backend.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Id;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
public class TusuariocontrasenaPK implements Serializable {
    @Column(name = "cusuario", nullable = false, length = 30)
    @Id
    private String cusuario;
    @Column(name = "fhasta", columnDefinition = "datetime DEFAULT '2999-12-31 00:00:00'")
    @Id
    private Timestamp fhasta;
}
