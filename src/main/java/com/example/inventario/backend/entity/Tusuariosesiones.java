package com.example.inventario.backend.entity;

import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.Where;

import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "tusuariosesiones", schema = "inventario", catalog = "")
@Data
@IdClass(TusuariosesionesPK.class)
@Where(clause = "fhasta='2999-12-31 00:00:00'")
public class Tusuariosesiones {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "cusuario", nullable = false, length = 30)
    private String cusuario;
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "fhasta", nullable = false)
    private Timestamp fhasta;
    @Basic
    @Column(name = "fdesde", nullable = false)
    private Timestamp fdesde;
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "sesion", nullable = false, length = 60)
    private String sesion;
    @Basic
    @Column(name = "ipadress", nullable = false, length = 15)
    private String ipadress;
}
