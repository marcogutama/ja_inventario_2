package com.example.inventario.backend.entity;

import jakarta.persistence.*;

import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "tsoftware", schema = "inventario", catalog = "")
@IdClass(com.example.inventario.backend.entity.TsoftwarePK.class)
public class Tsoftware {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @jakarta.persistence.Column(name = "csoftware", nullable = false)
    private int csoftware;

    public int getCsoftware() {
        return csoftware;
    }

    public void setCsoftware(int csoftware) {
        this.csoftware = csoftware;
    }

    @Basic
    @Column(name = "tipo", nullable = false, length = 30)
    private String tipo;

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Basic
    @Column(name = "nombre", nullable = true, length = 30)
    private String nombre;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "version", nullable = true, length = 20)
    private String version;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "fhasta", nullable = false)
    private Timestamp fhasta;

    public Timestamp getFhasta() {
        return fhasta;
    }

    public void setFhasta(Timestamp fhasta) {
        this.fhasta = fhasta;
    }

    @Basic
    @Column(name = "fdesde", nullable = false)
    private Timestamp fdesde;

    public Timestamp getFdesde() {
        return fdesde;
    }

    public void setFdesde(Timestamp fdesde) {
        this.fdesde = fdesde;
    }

    @Basic
    @Column(name = "cusuario", nullable = false, length = 20)
    private String cusuario;

    public String getCusuario() {
        return cusuario;
    }

    public void setCusuario(String cusuario) {
        this.cusuario = cusuario;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tsoftware tsoftware = (Tsoftware) o;
        return csoftware == tsoftware.csoftware && Objects.equals(tipo, tsoftware.tipo) && Objects.equals(nombre, tsoftware.nombre) && Objects.equals(version, tsoftware.version) && Objects.equals(fhasta, tsoftware.fhasta) && Objects.equals(fdesde, tsoftware.fdesde) && Objects.equals(cusuario, tsoftware.cusuario);
    }

    @Override
    public int hashCode() {
        return Objects.hash(csoftware, tipo, nombre, version, fhasta, fdesde, cusuario);
    }
}
