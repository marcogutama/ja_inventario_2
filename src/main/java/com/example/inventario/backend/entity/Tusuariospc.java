package com.example.inventario.backend.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "tusuariospc", schema = "inventario", catalog = "")
@IdClass(TusuariospcPK.class)
@Data
public class Tusuariospc {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "cusuariopc", nullable = false)
    private String cusuariopc;
    @Basic
    @Column(name = "usuario", nullable = false, length = 30)
    private String usuario;
    @Basic
    @Column(name = "nombre", nullable = true, length = 30)
    private String nombre;
    @Basic
    @Column(name = "apellido", nullable = true, length = 30)
    private String apellido;
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "fhasta", nullable = false)
    private Timestamp fhasta;
    @Basic
    @Column(name = "fdesde", nullable = false)
    private Timestamp fdesde;
    @Basic
    @Column(name = "cusuario", nullable = false, length = 20)
    private String cusuario;
}
