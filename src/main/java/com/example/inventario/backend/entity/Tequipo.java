package com.example.inventario.backend.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.Where;

import java.sql.Timestamp;

@Entity
@Table(name = "tequipo", schema = "inventario", catalog = "")
@Data
@IdClass(TequipoPK.class)
@Where(clause = "fhasta='2999-12-31 00:00:00'")
public class Tequipo {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "cequipo", nullable = false)
    private int cequipo;
    @Basic
    @Column(name = "cactivo", nullable = true, length = 22)
    private String cactivo;
    @Basic
    @NotNull
    @Column(name = "tipo", nullable = true, length = 30)
    private String tipo;
    @Basic
    @Column(name = "marca", nullable = true, length = 30)
    private String marca;
    @Basic
    @Column(name = "modelo", nullable = true, length = 30)
    private String modelo;
    @Basic
    @Column(name = "serial", nullable = true, length = 54)
    private String serial;
    @Basic
    @Column(name = "procesador", nullable = true, length = 44)
    private String procesador;
    @Basic
    @Column(name = "ram", nullable = true)
    private Integer ram;
    @Basic
    @Column(name = "discoduro", nullable = true)
    private Integer discoduro;
    @Basic
    @Column(name = "nombre", nullable = true, length = 23)
    private String nombre;
    @Basic
    @Column(name = "mac", nullable = true, length = 17)
    private String mac;
    @Basic
    @Column(name = "ip", nullable = true, length = 30)
    private String ip;
    @Basic
    @Column(name = "estado", nullable = true, length = 20)
    private String estado;
    @Basic
    @Column(name = "observacion", nullable = true, length = -1)
    private String observacion;
    @Basic
    @Column(name = "ubicacion", nullable = true, length = -1)
    private String ubicacion;
    @Basic
    @Column(name = "coficina", nullable = true)
    private Integer coficina;
    @Basic
    @Column(name = "cdepartamento", nullable = true)
    private Integer cdepartamento;
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "fhasta", nullable = false)
    private Timestamp fhasta;
    @Basic
    @Column(name = "fdesde", nullable = false)
    private Timestamp fdesde;
    @Basic
    @Column(name = "cusuario", nullable = false, length = 20)
    private String cusuario;

    @ManyToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumns(foreignKey = @ForeignKey(name = "none"), value = {
            @JoinColumn( name="coficina", referencedColumnName="coficina", insertable=false, updatable=false),
            @JoinColumn( name = "fhasta", referencedColumnName = "fhasta", insertable=false, updatable=false )
    })
    private Toficina oficina;

    @ManyToOne
    @JoinColumns(foreignKey = @ForeignKey(name = "none"), value = {
            @JoinColumn( name="cdepartamento", referencedColumnName="cdepartamento", insertable=false, updatable=false),
            @JoinColumn( name = "fhasta", referencedColumnName = "fhasta", insertable=false, updatable=false )
    })
    private Tdepartamento departamento;
}
