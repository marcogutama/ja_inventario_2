package com.example.inventario.backend.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.sql.Timestamp;

@Entity
@Table(name = "tmaterial", schema = "inventario", catalog = "")
@Data
@IdClass(TmaterialPK.class)
public class Tmaterial {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @jakarta.persistence.Column(name = "cmaterial", nullable = false)
    private int cmaterial;
    @Basic
    @Column(name = "codigo", nullable = false, length = 20)
    private String codigo;
    @Basic
    @Column(name = "descripcion", nullable = false, length = 50)
    private String descripcion;
    @Basic
    @Column(name = "czona", nullable = true)
    private Integer czona;
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "fhasta", nullable = false)
    private Timestamp fhasta;
    @Basic
    @Column(name = "fdesde", nullable = false)
    private Timestamp fdesde;
    @Basic
    @Column(name = "cusuario", nullable = false, length = 20)
    private String cusuario;
}
