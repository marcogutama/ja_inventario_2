package com.example.inventario.backend.entity;

import jakarta.persistence.*;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "tmovimientos", schema = "inventario", catalog = "")
@IdClass(com.example.inventario.backend.entity.TmovimientosPK.class)
public class Tmovimientos {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @jakarta.persistence.Column(name = "cmovimiento", nullable = false)
    private int cmovimiento;

    public int getCmovimiento() {
        return cmovimiento;
    }

    public void setCmovimiento(int cmovimiento) {
        this.cmovimiento = cmovimiento;
    }

    @Basic
    @Column(name = "cmaterial", nullable = false)
    private int cmaterial;

    public int getCmaterial() {
        return cmaterial;
    }

    public void setCmaterial(int cmaterial) {
        this.cmaterial = cmaterial;
    }

    @Basic
    @Column(name = "fmovimiento", nullable = false)
    private Date fmovimiento;

    public Date getFmovimiento() {
        return fmovimiento;
    }

    public void setFmovimiento(Date fmovimiento) {
        this.fmovimiento = fmovimiento;
    }

    @Basic
    @Column(name = "tipomovimiento", nullable = true, length = 30)
    private String tipomovimiento;

    public String getTipomovimiento() {
        return tipomovimiento;
    }

    public void setTipomovimiento(String tipomovimiento) {
        this.tipomovimiento = tipomovimiento;
    }

    @Basic
    @Column(name = "cantidad", nullable = true)
    private Integer cantidad;

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    @Basic
    @Column(name = "stock", nullable = true)
    private Integer stock;

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    @Basic
    @Column(name = "observacion", nullable = true, length = 100)
    private String observacion;

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "fhasta", nullable = false)
    private Timestamp fhasta;

    public Timestamp getFhasta() {
        return fhasta;
    }

    public void setFhasta(Timestamp fhasta) {
        this.fhasta = fhasta;
    }

    @Basic
    @Column(name = "fdesde", nullable = false)
    private Timestamp fdesde;

    public Timestamp getFdesde() {
        return fdesde;
    }

    public void setFdesde(Timestamp fdesde) {
        this.fdesde = fdesde;
    }

    @Basic
    @Column(name = "cusuario", nullable = false, length = 20)
    private String cusuario;

    public String getCusuario() {
        return cusuario;
    }

    public void setCusuario(String cusuario) {
        this.cusuario = cusuario;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tmovimientos that = (Tmovimientos) o;
        return cmovimiento == that.cmovimiento && cmaterial == that.cmaterial && Objects.equals(fmovimiento, that.fmovimiento) && Objects.equals(tipomovimiento, that.tipomovimiento) && Objects.equals(cantidad, that.cantidad) && Objects.equals(stock, that.stock) && Objects.equals(observacion, that.observacion) && Objects.equals(fhasta, that.fhasta) && Objects.equals(fdesde, that.fdesde) && Objects.equals(cusuario, that.cusuario);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cmovimiento, cmaterial, fmovimiento, tipomovimiento, cantidad, stock, observacion, fhasta, fdesde, cusuario);
    }
}
