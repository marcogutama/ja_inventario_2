package com.example.inventario.backend.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.Where;

import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "tusuariocontrasena", schema = "inventario", catalog = "")
@Data
@IdClass(TusuariocontrasenaPK.class)
@Where(clause = "fhasta='2999-12-31 00:00:00'")
public class Tusuariocontrasena {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "cusuario", nullable = false, length = 30)
    private String cusuario;
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "fhasta", nullable = false)
    private Timestamp fhasta;
    @Basic
    @Column(name = "fdesde", nullable = false)
    private Timestamp fdesde;
    @Basic
    @NotNull
    @Column(name = "contrasena", nullable = false, length = 60)
    private String contrasena;
    @Basic
    @Column(name = "fcaducidad", nullable = true)
    private Timestamp fcaducidad;
    @Basic
    @Column(name = "temporal", nullable = true)
    private Byte temporal;

    @Override
    public String toString() {
        return getContrasena();
    }
}
