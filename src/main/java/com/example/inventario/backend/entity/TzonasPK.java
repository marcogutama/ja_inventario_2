package com.example.inventario.backend.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Id;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
public class TzonasPK implements Serializable {
    @Column(name = "czona")
    @Id
    private int czona;
    @Column(name = "fhasta", columnDefinition = "datetime DEFAULT '2999-12-31 00:00:00'")
    @Id
    private Timestamp fhasta;
}
