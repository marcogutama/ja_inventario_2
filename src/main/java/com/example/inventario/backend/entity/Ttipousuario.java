package com.example.inventario.backend.entity;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Table(name = "ttipousuario", schema = "inventario", catalog = "")
@Data
public class Ttipousuario {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "ctipousuario", nullable = false, length = 3)
    private String ctipousuario;
    @Basic
    @Column(name = "descripcion", nullable = false, length = 255)
    private String descripcion;

    @Override
    public String toString() {
        return getCtipousuario();
    }
}
