package com.example.inventario.backend.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.Where;

import java.sql.Timestamp;

@Entity
@Table(name = "tusuarios", schema = "inventario", catalog = "")
@Data
@IdClass(TusuariosPK.class)
@Where(clause = "fhasta='2999-12-31 00:00:00'")
public class Tusuarios {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "cusuario", nullable = false, length = 30)
    private String cusuario;
    @Basic
    @Column(name = "nombre", nullable = true, length = 30)
    private String nombre;
    @Basic
    @Column(name = "czona", nullable = true)
    private int czona;
    @Basic
    @NotNull
    @Column(name = "ctipousuario", nullable = true, length = 3)
    private String ctipousuario;
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "fhasta", nullable = false)
    private Timestamp fhasta;
    @Basic
    @Column(name = "fdesde", nullable = false)
    private Timestamp fdesde;
    @Basic
    @Column(name = "cusuario_modificacion", nullable = true, length = 30)
    private String cusuarioModificacion;
}
