package com.example.inventario.backend.entity;

import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;

import java.sql.Timestamp;
import java.util.Objects;

@Entity
@jakarta.persistence.Table(name = "tequipos_software", schema = "inventario", catalog = "")
public class TequiposSoftware {
    @Id
    @Basic
    @Column(name = "cequipo", nullable = true)
    private Integer cequipo;

    public Integer getCequipo() {
        return cequipo;
    }

    public void setCequipo(Integer cequipo) {
        this.cequipo = cequipo;
    }

    @Basic
    @jakarta.persistence.Column(name = "cusuariopc", nullable = true, length = 30)
    private String cusuariopc;

    public String getCusuariopc() {
        return cusuariopc;
    }

    public void setCusuariopc(String cusuariopc) {
        this.cusuariopc = cusuariopc;
    }

    @Basic
    @jakarta.persistence.Column(name = "csoftware", nullable = true)
    private Integer csoftware;

    public Integer getCsoftware() {
        return csoftware;
    }

    public void setCsoftware(Integer csoftware) {
        this.csoftware = csoftware;
    }

    @Basic
    @jakarta.persistence.Column(name = "licencia", nullable = true, length = 30)
    private String licencia;

    public String getLicencia() {
        return licencia;
    }

    public void setLicencia(String licencia) {
        this.licencia = licencia;
    }

    @Basic
    @jakarta.persistence.Column(name = "fhasta", nullable = false)
    private Timestamp fhasta;

    public Timestamp getFhasta() {
        return fhasta;
    }

    public void setFhasta(Timestamp fhasta) {
        this.fhasta = fhasta;
    }

    @Basic
    @jakarta.persistence.Column(name = "fdesde", nullable = false)
    private Timestamp fdesde;

    public Timestamp getFdesde() {
        return fdesde;
    }

    public void setFdesde(Timestamp fdesde) {
        this.fdesde = fdesde;
    }

    @Basic
    @jakarta.persistence.Column(name = "cusuario", nullable = false, length = 20)
    private String cusuario;

    public String getCusuario() {
        return cusuario;
    }

    public void setCusuario(String cusuario) {
        this.cusuario = cusuario;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TequiposSoftware that = (TequiposSoftware) o;
        return Objects.equals(cequipo, that.cequipo) && Objects.equals(cusuariopc, that.cusuariopc) && Objects.equals(csoftware, that.csoftware) && Objects.equals(licencia, that.licencia) && Objects.equals(fhasta, that.fhasta) && Objects.equals(fdesde, that.fdesde) && Objects.equals(cusuario, that.cusuario);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cequipo, cusuariopc, csoftware, licencia, fhasta, fdesde, cusuario);
    }
}
