package com.example.inventario.backend.entity;

import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.Where;

import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "toficina", schema = "inventario", catalog = "")
@Data
@IdClass(ToficinaPK.class)
@Where(clause = "fhasta='2999-12-31 00:00:00'")
public class Toficina {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "coficina", nullable = false)
    private int coficina;
    @Basic
    @Column(name = "nombre", nullable = false, length = 30)
    private String nombre;
    @Basic
    @Column(name = "abreviatura", nullable = true, length = 3)
    private String abreviatura;
    @Basic
    @Column(name = "zona", nullable = true, length = 20)
    private String zona;
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "fhasta", nullable = false)
    private Timestamp fhasta;
    @Basic
    @Column(name = "fdesde", nullable = false)
    private Timestamp fdesde;
    @Basic
    @Column(name = "cusuario", nullable = false, length = 20)
    private String cusuario;
    @Override
    public String toString() {
        return getNombre();
    }
}
