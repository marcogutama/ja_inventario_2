package com.example.inventario.backend.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.sql.Date;
import java.sql.Timestamp;

@Entity
@Table(name = "tglpi", schema = "inventario", catalog = "")
@Data
@IdClass(com.example.inventario.backend.entity.TglpiPK.class)
public class Tglpi {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "cglpi", nullable = false)
    private int cglpi;
    @Basic
    @Column(name = "cequipo", nullable = true)
    private Integer cequipo;
    @Basic
    @Column(name = "fecha", nullable = false)
    private Date fecha;
    @Basic
    @Column(name = "numero", nullable = true, length = 10)
    private String numero;
    @Basic
    @Column(name = "descripcion", nullable = true, length = -1)
    private String descripcion;
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "fhasta", nullable = false)
    private Timestamp fhasta;
    @Basic
    @Column(name = "fdesde", nullable = false)
    private Timestamp fdesde;
    @Basic
    @Column(name = "cusuario", nullable = false, length = 20)
    private String cusuario;
}
