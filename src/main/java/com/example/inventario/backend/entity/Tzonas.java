package com.example.inventario.backend.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "tzonas", schema = "inventario", catalog = "")
@IdClass(com.example.inventario.backend.entity.TzonasPK.class)
@Data
public class Tzonas {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "czona", nullable = false)
    private int czona;
    @Basic
    @Column(name = "nombre", nullable = false, length = 31)
    private String nombre;
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "fhasta", nullable = false)
    private Timestamp fhasta;
    @Basic
    @Column(name = "fdesde", nullable = false)
    private Timestamp fdesde;
    @Basic
    @Column(name = "cusuario", nullable = false, length = 20)
    private String cusuario;
}
