package com.example.inventario.backend.entity;

import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.Where;

import java.sql.Timestamp;

@Entity
@Table(name = "tdepartamento", schema = "inventario", catalog = "")
@Data
@IdClass(TdepartamentoPK.class)
@Where(clause = "fhasta='2999-12-31 00:00:00'")
public class Tdepartamento {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "cdepartamento", nullable = false)
    private int cdepartamento;
    @Basic
    @Column(name = "nombre", nullable = false, length = 31)
    private String nombre;
    @Basic
    @Column(name = "abreviatura", nullable = true, length = 3)
    private String abreviatura;
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "fhasta", nullable = false)
    private Timestamp fhasta;
    @Basic
    @Column(name = "fdesde", nullable = false)
    private Timestamp fdesde;
    @Basic
    @Column(name = "cusuario", nullable = false, length = 20)
    private String cusuario;
    @Override
    public String toString() {
        return getNombre();
    }
}
