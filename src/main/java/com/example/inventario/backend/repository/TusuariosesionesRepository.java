package com.example.inventario.backend.repository;

import com.example.inventario.backend.entity.Tusuariosesiones;
import com.example.inventario.backend.entity.TusuariosesionesPK;

public interface TusuariosesionesRepository extends ExtendedRepository <Tusuariosesiones, TusuariosesionesPK>{
}
