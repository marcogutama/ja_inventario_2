package com.example.inventario.backend.repository;

import com.example.inventario.backend.entity.Tusuarios;
import com.example.inventario.backend.entity.TusuariosPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TusuariosRepository extends ExtendedRepository<Tusuarios, TusuariosPK> {
    Tusuarios findByCusuario(String usuario);
}