package com.example.inventario.backend.repository;

import com.example.inventario.backend.entity.Tusuariocontrasena;
import com.example.inventario.backend.entity.TusuariocontrasenaPK;

public interface TusuariocontrasenaRepository extends ExtendedRepository<Tusuariocontrasena, TusuariocontrasenaPK> {
    Tusuariocontrasena findByCusuario(String usuario);
}
