package com.example.inventario.backend.repository;

import com.example.inventario.backend.entity.Tsoftware;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import java.util.Date;

public interface TsoftwareRepository extends JpaRepository<Tsoftware, Date>, JpaSpecificationExecutor<Tsoftware> {

}