package com.example.inventario.backend.repository;

import com.example.inventario.backend.entity.Tusuariospc;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import java.util.Date;

public interface TusuariospcRepository extends JpaRepository<Tusuariospc, Date>, JpaSpecificationExecutor<Tusuariospc> {

}