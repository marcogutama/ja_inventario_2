package com.example.inventario.backend.repository;

import com.example.inventario.backend.entity.Tzonas;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import java.util.Date;

public interface TzonasRepository extends JpaRepository<Tzonas, Date>, JpaSpecificationExecutor<Tzonas> {

}