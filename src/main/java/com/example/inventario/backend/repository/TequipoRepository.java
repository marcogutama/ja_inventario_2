package com.example.inventario.backend.repository;

import com.example.inventario.backend.entity.Tequipo;
import com.example.inventario.backend.entity.TequipoPK;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

@Repository
public interface TequipoRepository extends ExtendedRepository<Tequipo, TequipoPK> {
    @EntityGraph(
            type = EntityGraph.EntityGraphType.FETCH,
            attributePaths = {"oficina", "departamento"}
    )
    List<Tequipo> findAll();

    @EntityGraph(
            type = EntityGraph.EntityGraphType.FETCH,
            attributePaths = {"oficina", "departamento"}
    )
    List<Tequipo> findByParameters(HashMap<String, Object> parametros);
}
