package com.example.inventario.backend.repository;

import com.example.inventario.backend.entity.Tmovimientos;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import java.util.Date;

public interface TmovimientosRepository extends JpaRepository<Tmovimientos, Date>, JpaSpecificationExecutor<Tmovimientos> {

}