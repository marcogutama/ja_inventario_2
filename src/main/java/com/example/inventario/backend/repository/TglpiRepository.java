package com.example.inventario.backend.repository;

import com.example.inventario.backend.entity.Tglpi;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Date;

public interface TglpiRepository extends JpaRepository<Tglpi, Date>, JpaSpecificationExecutor<Tglpi> {

}