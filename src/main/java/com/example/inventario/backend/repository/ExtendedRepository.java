package com.example.inventario.backend.repository;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;


@NoRepositoryBean
public interface ExtendedRepository<T, ID extends Serializable> extends JpaRepository<T, ID> {

    public List<T> findAllActual();
    public void expire(T entity);
    public <S extends T> S saveWithAudit(S entity);
    public <S extends T> S update(S entity);
    public List<T> findByParameters(HashMap<String, Object> parametrosBusqueda);
}
