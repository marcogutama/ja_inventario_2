package com.example.inventario.backend.repository;

import com.example.inventario.backend.entity.Toficina;
import com.example.inventario.backend.entity.ToficinaPK;
import org.springframework.stereotype.Repository;

@Repository
public interface ToficinaRepository extends ExtendedRepository<Toficina, ToficinaPK> {
}
