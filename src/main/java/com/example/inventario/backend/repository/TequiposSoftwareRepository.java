package com.example.inventario.backend.repository;

import com.example.inventario.backend.entity.TequiposSoftware;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface TequiposSoftwareRepository extends JpaRepository<TequiposSoftware, Void>, JpaSpecificationExecutor<TequiposSoftware> {

}