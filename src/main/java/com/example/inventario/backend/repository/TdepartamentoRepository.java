package com.example.inventario.backend.repository;

import com.example.inventario.backend.entity.Tdepartamento;
import com.example.inventario.backend.entity.TdepartamentoPK;
import org.springframework.stereotype.Repository;

@Repository
public interface TdepartamentoRepository extends ExtendedRepository<Tdepartamento, TdepartamentoPK> {
}
