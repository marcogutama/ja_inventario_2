package com.example.inventario.backend.repository;

import com.example.inventario.backend.entity.Ttipousuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface TtipousuarioRepository extends JpaRepository<Ttipousuario, Integer>, JpaSpecificationExecutor<Ttipousuario> {

}