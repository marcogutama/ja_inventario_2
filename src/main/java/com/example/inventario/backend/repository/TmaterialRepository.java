package com.example.inventario.backend.repository;

import com.example.inventario.backend.entity.Tmaterial;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Date;

public interface TmaterialRepository extends JpaRepository<Tmaterial, Date>, JpaSpecificationExecutor<Tmaterial> {

}