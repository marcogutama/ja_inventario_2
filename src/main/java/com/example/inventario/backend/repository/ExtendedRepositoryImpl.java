package com.example.inventario.backend.repository;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.*;

import com.example.inventario.backend.ApplicationDates;
import com.example.inventario.security.SecurityService;
import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.JpaEntityInformationSupport;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.sql.Timestamp;

public class ExtendedRepositoryImpl<T, ID extends Serializable> extends SimpleJpaRepository<T, ID> implements ExtendedRepository<T, ID> {

    private EntityManager entityManager;
    private final JpaEntityInformation<T, ?> entityInformation;
    @Autowired
    SecurityService securityService;

    public ExtendedRepositoryImpl(JpaEntityInformation<T, ?> entityInformation, EntityManager entityManager) {
        super(entityInformation, entityManager);
        this.entityManager = entityManager;
        this.entityInformation = JpaEntityInformationSupport.getEntityInformation(getDomainClass(), entityManager);
    }

    @Transactional
    public List<T> findAllActual() {
        return entityManager.createQuery(
                "Select t from " + getDomainClass().getSimpleName() + " t where t.fhasta=:expire")
                .setParameter("expire", ApplicationDates.DEFAULT_EXPIRY_TIMESTAMP)
                .getResultList();
    }

    @Transactional
    public List<T> findByParameters(HashMap<String, Object> parametros) {
        if(parametros.size()==0){
            return super.findAll();
        }
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> query = builder.createQuery(getDomainClass());
        Root<T> root = query.from(getDomainClass());
        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<Predicate>();
        for (String parametro:parametros.keySet()) {
            String valor = parametros.get(parametro).toString();
            if(!valor.equals("")){
                predicates.add(builder.like(root.<String>get(parametro), "%" + valor + "%"));
            }
        }
        query.select(root).where(predicates.toArray(new Predicate[]{}));
        TypedQuery<T> q = entityManager.createQuery(query);
        return q.getResultList();
    }

    @Transactional
    public void expire(T entity) {
        //Assert.notNull(entity, "The entity must not be null!");
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaUpdate<T> update = cb.createCriteriaUpdate((Class<T>) getDomainClass());
        Root<T> root = update.from((Class<T>) getDomainClass());

        update.set("fhasta", new Timestamp((new Date()).getTime()));

        final List<Predicate> predicates = new ArrayList<Predicate>();
        if (entityInformation.hasCompositeId()) {
            for (String s : entityInformation.getIdAttributeNames())
                predicates.add(cb.equal(root.<ID>get(s),
                        entityInformation.getCompositeIdAttributeValue(entityInformation.getId(entity), s)));
            update.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));
        } else
            update.where(cb.equal(root.<ID>get(entityInformation.getIdAttribute().getName()),
                    entityInformation.getId(entity)));

        entityManager.createQuery(update).executeUpdate();
    }

    @Transactional
    public <S extends T> S saveWithAudit(S entity){
        Assert.notNull(entity, "Entity must not be null");
        try {
            entity= (S) setValue(entity, "fhasta", ApplicationDates.DEFAULT_EXPIRY_TIMESTAMP);
            entity= (S) setValue(entity, "cusuario", securityService.getAuthenticatedUser().getUsername());
            entity= (S) setValue(entity, "fdesde", ApplicationDates.CURRENT_EXPIRY_TIMESTAMP);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
        entityManager.persist(entity);
        return entity;
    }

    /**
     * Metodo para setear valores por default en la entidad
     * @param classObject
     * @param fieldIdentifier
     * @param value
     * @return
     * @throws IllegalAccessException
     */
    public Object setValue(Object classObject, String fieldIdentifier, Object value) throws IllegalAccessException {
        Class<?> clazz = classObject.getClass();
        Field retrievedField = Arrays.stream(clazz.getDeclaredFields())
                .filter(field -> field.getName().equalsIgnoreCase(fieldIdentifier))
                .findFirst().orElse(null);//add an exception here when null

        retrievedField.setAccessible(true);
        retrievedField.set(classObject, value);
        return classObject;
    }

    @Transactional
    public <S extends T> S update(S entity){
        expire(entity);
        return saveWithAudit(entity);
    }

}
