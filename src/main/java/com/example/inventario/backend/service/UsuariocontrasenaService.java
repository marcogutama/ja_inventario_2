package com.example.inventario.backend.service;

import com.example.inventario.backend.entity.Tusuariocontrasena;
import com.example.inventario.backend.entity.Tusuarios;
import com.example.inventario.backend.repository.TusuariocontrasenaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UsuariocontrasenaService {
    private final TusuariocontrasenaRepository repository;

    public Tusuariocontrasena add(Tusuariocontrasena tusuariocontrasena) {
        Tusuariocontrasena usuario = repository.findByCusuario(tusuariocontrasena.getCusuario());
        if (usuario != null) {repository.expire(tusuariocontrasena);}
        
        return repository.save(tusuariocontrasena);
    }

    public Tusuariocontrasena findByCusuario(String cusuario) {
        return repository.findByCusuario(cusuario);
    }

    public Tusuariocontrasena update(Tusuariocontrasena tusuarios) {
        repository.expire(tusuarios);
        return repository.save(tusuarios);
    }

}
