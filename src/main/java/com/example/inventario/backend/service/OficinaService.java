package com.example.inventario.backend.service;

import com.example.inventario.backend.entity.Toficina;
import com.example.inventario.backend.repository.ToficinaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.vaadin.crudui.crud.CrudListener;

import java.util.Collection;
import java.util.HashMap;

@Service
@RequiredArgsConstructor
public class OficinaService implements CrudListener<Toficina>  {
    private final ToficinaRepository repository;

    public Collection<Toficina> findAll() {
        return repository.findAll();
    }

    public Collection<Toficina> findAll(HashMap<String, Object> parametrosBusqueda) {
        return repository.findByParameters(parametrosBusqueda);
    }

    public Toficina add(Toficina oficina) {
        return repository.saveWithAudit(oficina);
    }

    public Toficina update(Toficina oficina) {
        return repository.update(oficina);
    }

    public void delete(Toficina oficina) {
        repository.expire(oficina);
    }
}
