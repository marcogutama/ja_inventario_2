package com.example.inventario.backend.service;

import com.example.inventario.backend.entity.Tusuariosesiones;
import com.example.inventario.backend.repository.TusuariosesionesRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TusuariosesionesService {
    private final TusuariosesionesRepository repository;

    public Tusuariosesiones add (Tusuariosesiones tusuariosesiones) {
        return repository.save(tusuariosesiones);
    }
}
