package com.example.inventario.backend.service;

import com.example.inventario.backend.entity.Tdepartamento;
import com.example.inventario.backend.repository.TdepartamentoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
@RequiredArgsConstructor
public class DepartamentoService {
    private final TdepartamentoRepository repository;

    public Collection<Tdepartamento> findAll() {
        return repository.findAll();
    }

}
