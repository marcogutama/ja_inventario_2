package com.example.inventario.backend.service;

import com.example.inventario.backend.ApplicationDates;
import com.example.inventario.backend.entity.Tusuarios;
import com.example.inventario.backend.repository.TusuariosRepository;
import com.example.inventario.security.SecurityService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.vaadin.crudui.crud.CrudListener;

import java.util.Collection;
import java.util.HashMap;

@Service
@RequiredArgsConstructor
public class UsuarioService implements CrudListener<Tusuarios> {
    private final TusuariosRepository repository;
    @Autowired
    SecurityService securityService;
    @Override
    public Collection<Tusuarios> findAll() {
        return repository.findAll();
    }

    public Collection<Tusuarios> findAll(HashMap<String, Object> parametrosBusqueda) {
        return repository.findByParameters(parametrosBusqueda);
    }

    @Override
    public Tusuarios add(Tusuarios tusuarios) {
        return repository.save(audit(tusuarios));
    }

    @Override
    public Tusuarios update(Tusuarios tusuarios) {
        repository.expire(tusuarios);
        return repository.save(audit(tusuarios));
    }

    /**
     * Se crea metodo propio audit, ya que no se puede utiliar
     * metodo por defecto, ya que cusuario ahora es cusuario_modificacion
     * @param tusuarios
     * @return
     */
    public Tusuarios audit(Tusuarios tusuarios) {
        tusuarios.setFhasta(ApplicationDates.DEFAULT_EXPIRY_TIMESTAMP);
        tusuarios.setFdesde(ApplicationDates.CURRENT_EXPIRY_TIMESTAMP);
        tusuarios.setCusuarioModificacion(securityService.getAuthenticatedUser().getUsername());
        return tusuarios;
    }

    @Override
    public void delete(Tusuarios tusuarios) {
        repository.expire(tusuarios);
    }
}
