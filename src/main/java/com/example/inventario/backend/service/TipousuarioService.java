package com.example.inventario.backend.service;

import com.example.inventario.backend.entity.Ttipousuario;
import com.example.inventario.backend.repository.TtipousuarioRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
@RequiredArgsConstructor
public class TipousuarioService {
    private final TtipousuarioRepository repository;

    public Collection<Ttipousuario> findAll() {
        return repository.findAll();
    }
}
