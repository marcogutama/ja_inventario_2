package com.example.inventario.backend.service;

import com.example.inventario.backend.entity.Tusuariocontrasena;
import com.example.inventario.backend.entity.Tusuarios;
import com.example.inventario.backend.repository.TusuariocontrasenaRepository;
import com.example.inventario.backend.repository.TusuariosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UsuarioDetailsService implements UserDetailsService {
    @Autowired
    private TusuariosRepository userRepository;
    @Autowired
    private TusuariocontrasenaRepository userPassRepository;

    @Override
    public UserDetails loadUserByUsername(String usuario) {
        Tusuarios user = userRepository.findByCusuario(usuario);
        Tusuariocontrasena userPass = userPassRepository.findByCusuario(usuario);
        if (user == null || userPass == null) {
            throw new UsernameNotFoundException(usuario);
        }
        return new User(user.getCusuario(), userPass.getContrasena(), true, true,
                userPass.getTemporal()==0, true, getRoles(user.getCtipousuario()));
    }

    private Collection<? extends GrantedAuthority> getRoles(String ctipousuario) {
        Set<GrantedAuthority> roles = new HashSet<>();
        roles.add(new SimpleGrantedAuthority("ROLE_" + ctipousuario));
        return roles;
    }
}
