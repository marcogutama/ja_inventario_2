package com.example.inventario.backend.service;

import com.example.inventario.backend.entity.Tequipo;
import com.example.inventario.backend.repository.TequipoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.vaadin.crudui.crud.CrudListener;
import java.util.Collection;
import java.util.HashMap;

@Service
@RequiredArgsConstructor
public class EquipoService implements CrudListener<Tequipo> {
    private final TequipoRepository repository;
    @Override
    public Collection<Tequipo> findAll() {
        return repository.findAll();
    }

    public Collection<Tequipo> findAll(HashMap<String, Object> parametrosBusqueda) {
        return repository.findByParameters(parametrosBusqueda);
    }

    @Override
    public Tequipo add(Tequipo equipo) {
        return repository.saveWithAudit(equipo);
    }

    @Override
    public Tequipo update(Tequipo equipo) {
        equipo.setCoficina(equipo.getOficina()!=null?equipo.getOficina().getCoficina():null);
        return repository.update(equipo);
    }

    @Override
    public void delete(Tequipo equipo) {
        repository.expire(equipo);
    }
}
