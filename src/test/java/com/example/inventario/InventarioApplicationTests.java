package com.example.inventario;

import com.example.inventario.backend.entity.TusuariosPK;
import com.example.inventario.backend.repository.TusuariosRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.Timestamp;
import java.util.Date;

@SpringBootTest
class InventarioApplicationTests {
    @Autowired
    private TusuariosRepository userRepository;
    @Test
    void contextLoads() {
        TusuariosPK tusuariosPK = new TusuariosPK();
        tusuariosPK.setCusuario("admin");
        tusuariosPK.setFhasta(new Timestamp((new Date()).getTime()));
        System.out.println(""+userRepository.findById(tusuariosPK));;

    }

}
